#pragma once
#include "FileStream.h"

class Logger
{
public:
	Logger(const char *filename, bool logToScreen);
	~Logger();


	void print(const char *msg);
private:
	FILE* _fp;
	bool _logToScreen;
	OutStream _os;
	FileStream _fs;
	static unsigned int _counter;
};
