#pragma once
#include "OutStream.h"
class OutStreamEncrypted : public OutStream
{
private:
	int _ist;
public:
	OutStreamEncrypted(int ist);
	~OutStreamEncrypted();
	OutStreamEncrypted& operator<<(const char* str);
};