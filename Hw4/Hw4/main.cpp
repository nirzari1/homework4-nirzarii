#include "OutStream.h"
#include "FileStream.h"
#include "OutStreamEncrypted.h"
#include "Logger.h"

const char* str(const char* string);

unsigned int Logger::_counter = 1; // question 5
int main(int argc, char **argv)
{
	OutStream os;
	const char* (*str_ptr)(const char*) = &str;
	os << "I am the Doctor and I'm " << 1500 << (*str_ptr)(" years old") << endline;
	     // str print               // int print // str print through pointer to function // endline - 4 funcs

	FileStream a("doctor.txt");
	a << "I am the doctor and I'm 1500 years oldd\n"; // question 2

	OutStreamEncrypted b(3);
	b << "abcdefghijklmnopqrstuvwxyz"; // question 3
	os << "\n";
		
	Logger c("nirnir.txt", true);
	Logger d("nirnir1.txt", true);
	c.print("supposed to be line 1!!\n");
	c.print("supposed to be line 2\n");
	c.print("supposed to be line 3\n"); // question 4
	d.print("supposed to be line 4!\n");
	d.print("line 5?\n");
	return 0;
}
/*
This function gets a string and returns the string
*/
const char* str(const char* string)
{
	return string;
}