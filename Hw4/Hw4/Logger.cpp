#include "Logger.h"
void Logger::print(const char* msg)
{
	_fs << "Logger writing, times written to file: ";
	_fs << _counter << "\n"; // writes
	_fs << msg << "\n"; // writing to file that the logger is writing to the file and then writes the msg
	if (_logToScreen == true)
	{
		fprintf(stdout, "Line %d: ", _counter);
		fprintf(stdout, msg); // if logstoscreen is true, writes to screen aswell
	}
	_counter++;
}
Logger::~Logger()
{
	if (_fp != stdout && _fp != nullptr)
	{
		fclose(_fp); // if _fp was opened, it closes it
	}
}
Logger::Logger(const char* filename, bool logToScreen) : _fs(filename) // initilzation constructor for file stream
{
	fopen_s(&_fp, filename, "w"); // This function opens the file to write mode so u can write stuff to it
	_logToScreen = logToScreen;
}