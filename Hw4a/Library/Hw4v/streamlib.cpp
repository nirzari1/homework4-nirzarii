#include "streamlib.h"
#include <stdio.h>
using namespace msl;
OutStream::OutStream()
{
}

OutStream::~OutStream()
{
}

OutStream& OutStream::operator<<(const char* str)
{
	fprintf(f, str); // fprintf on stdcout (default of f) will be equal to printf
	return *this;
}

OutStream& OutStream::operator<<(int num)
{
	fprintf(f, "%d", num); // fprintf on stdout(default of f) will be equal to printf
	return *this;
}

OutStream& OutStream::operator<<(void(*pf)())
{
	pf();
	return *this;
}

