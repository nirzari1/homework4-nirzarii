#pragma once
#include <fstream>
namespace msl
{
	class OutStream
	{
	protected:
		FILE* f = stdout; // default value of f will always be stdout (fprintf on stdout makes it printf)
	public:
		OutStream();
		~OutStream();

		OutStream& operator<<(const char* str);
		OutStream& operator<<(int num);
		OutStream& operator<<(void(*pf)());
	};

}
